package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import bufmgr.PageNotReadException;
import global.AttrType;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Tuple;
import index.IndexException;
import iterator.CondExpr;
import iterator.FileScan;
import iterator.FldSpec;
import iterator.IESelfJoinOnePred;
import iterator.IESelfJoinTwoPred;
import iterator.Iterator;
import iterator.JoinSelf;
import iterator.JoinsException;
import iterator.LowMemException;
import iterator.NestedLoopsJoins;
import iterator.PredEvalException;
import iterator.RelSpec;
import iterator.SortException;
import iterator.TupleUtilsException;
import iterator.UnknowAttrType;
import iterator.UnknownKeyTypeException;

public class QueryTrait2 {
	private static final boolean FAIL = false;
	private static final boolean OK = true;

	public static void execute(String query_file, int max, String iterator) throws JoinsException, IndexException, InvalidTupleSizeException, InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {
		
		boolean status = OK;
		long startTime = System.currentTimeMillis();

		
			// First we try to read the query file in order to extract
			// relations names, projection and condition columns
			JoinsDriver_ jjoin = new JoinsDriver_(max);
			
			
			QueryAnalyzer query_past = new QueryAnalyzer(query_file);
		    CondExpr[] outFilter = query_past.getOutFilter();
			   
		    Integer[] column_selected = query_past.getColumn_selected();

			    
		    String[] relations = query_past.getRelations();
			    
		    String innerRelation = new String(relations[1]);
		    String outerRelation = new String(relations[0]);
			

			Tuple t = new Tuple();
			t = null;
			// Inner relation attribute types (all integers)
			AttrType types[] = {
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger)
			};

			// We probably won't need this because we are only working with integers
			short []   sizes = new short[1];
			sizes[0] = 0;

			// Select columns to work with
			// Those are not the final columns to show
			// but the ones we will be working with to do the join
		
			FldSpec [] proj = {
					new FldSpec(
							new RelSpec(RelSpec.outer), 1),
					new FldSpec(new RelSpec(RelSpec.outer), 2),
					new FldSpec(new RelSpec(RelSpec.outer), 3),
					new FldSpec(new RelSpec(RelSpec.outer), 4),
			};

			// Select columns to show (projection)
			// This piece of information is fetched from the query file
			FldSpec [] Projection = {
					new FldSpec(new RelSpec(RelSpec.outer), 
							column_selected[0]),
					new FldSpec(new RelSpec(RelSpec.innerRel),
							column_selected[1])
			};

			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			//***************  create a scan on the heapfile  ***************
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			// Open and scan one of the database as heapfile
			iterator.Iterator outer1 = null;
			iterator.Iterator outer2 = null;
			iterator.Iterator inner1 = null;
			iterator.Iterator inner2 = null;

			try {
				outer1 = new FileScan(outerRelation+ ".in", 
						types, sizes, (short) 4, (short) 4, proj, null);
				outer2 = new FileScan(outerRelation + ".in", 
						types, sizes, (short) 4, (short) 4, proj, null);
			} catch (Exception e) {
				status = FAIL;
				System.err.println("" + e);
			}

			try {
				inner1 = new FileScan(innerRelation + ".in", 
						types, sizes, (short) 4, (short) 4, proj, null);
				inner2 = new FileScan(innerRelation + ".in", 
						types, sizes, (short) 4, (short) 4, proj, null);
			} catch (Exception e) {
				status = FAIL;
				System.err.println("" + e);
			}

		
		
			Iterator ieJoin = null;
			JoinSelf ieJoinH = null;
			try {
				 if (iterator.equals("NLJ")) {
					ieJoin = new NestedLoopsJoins(types, 4, null, types, 4, null, 10, outer1, innerRelation+".in", outFilter, null, Projection, 2);
				  }
				 
				 if (iterator.equals("IESelfJoinTwoPred")) {
				    	
				    ieJoin = new IESelfJoinTwoPred(types, 4, sizes, 10,outer1, outer2, outFilter, null, Projection, 2);
				 }
				 
				 if (iterator.equals("JoinSelf")) {
				    	
					ieJoin = new JoinSelf(types, 4, sizes,types, 4, sizes, 10, outer1,outer2,
							inner1 , inner2 , outFilter, null, Projection, 2);
				 }
				 System.out.println("Iterator Used  "+ iterator);
				System.out.println("Execution time in ms of this" + query_file + " " + (System.currentTimeMillis() - startTime));
			}

			
			catch (Exception e) {
				System.err.println ("*** Error preparing for nested_loop_join");
				System.err.println (""+e);
				e.printStackTrace();
				Runtime.getRuntime().exit(1);
			}

			AttrType[] jtype = { new AttrType(AttrType.attrInteger), new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger), new AttrType(AttrType.attrInteger)};
		
			PrintWriter pw = new PrintWriter("output_test_ie.txt");
			t = null;
			int i = 0;
			try {
				while ((t = ieJoin.get_next()) != null) {
					i++;
					//t.print(jtype); // result print
					pw.print("[" + t.getIntFld(1) + ","  +  t.getIntFld(2) +  "]\n"); // output file
				}
				pw.close();
				// print the total number of returned tuples
				System.out.println("Output Tuples for " + query_file + ": " + i);
			} catch (Exception e) {
				System.err.println("" + e);
				e.printStackTrace();
				Runtime.getRuntime().exit(1);
			}

			try {
				ieJoin.close();
			} catch (Exception e) {
				status = FAIL;
				e.printStackTrace();
			}

			if (status != OK) {
				//bail out
				Runtime.getRuntime().exit(1);
			}

		
	}
	
}
