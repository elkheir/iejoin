package tests;

import java.util.concurrent.TimeUnit;

import iterator.*;
import heap.*;
import global.*;
import index.*;

// originally from : joins.C
import java.io.*;
import java.util.*;
import javax.xml.namespace.QName;
import java.lang.*;
import diskmgr.*;
import bufmgr.*;
import btree.*;
import catalog.*;


class R {
	  public int int_1;
	  public int int_2;
	  public int int_3;
	  public int int_4;

	  public R(int _int_1, int _int_2, int _int_3, int _int_4) {
	    int_1 = _int_1;
	    int_2 = _int_2;
	    int_3 = _int_3;
	    int_4 = _int_4;
	  }
	}


	// Define the S schema
class S {
	  public int int_1;
	  public int int_2;
	  public int int_3;
	  public int int_4;

	  public S(int _int_1, int _int_2, int _int_3, int _int_4) {
	    int_1 = _int_1;
	    int_2 = _int_2;
	    int_3 = _int_3;
	    int_4 = _int_4;
	  }
	}


	// Define the R schema
class Q {
	  public int int_1;
	  public int int_2;
	  public int int_3;
	  public int int_4;

	  public Q(int _int_1, int _int_2, int _int_3, int _int_4) {
	    int_1 = _int_1;
	    int_2 = _int_2;
	    int_3 = _int_3;
	    int_4 = _int_4;
	  }
	}

class JoinsDriver_ implements GlobalConst {

  private boolean OK = true;
  private boolean FAIL = false;
  private Vector s;
  private Vector r;
  private Vector q;
 
  
public JoinsDriver_(Integer maxRows) {
    // build R, S, Q tables
    r = new Vector();
    s = new Vector();
    q = new Vector();

    UsefulFunctions use = new UsefulFunctions();
   
    ArrayList<Integer> listr = use.tighten_rows("R.txt", maxRows);
    int rows_r = use.getNumRows();
    ArrayList<Integer> lists = use.tighten_rows("S.txt", maxRows);
    int rows_s = use.getNumRows();
    ArrayList<Integer> listq = use.tighten_rows("Q.txt", maxRows);
    int rows_q = use.getNumRows();
    
    
    for (int i =0; i<=4*maxRows-4;i=i+4) {
    	
    	if(i<=4*rows_r-4) {
    	r.addElement(new R(listr.get(i), listr.get(i+1), listr.get(i+2), listr.get(i+3)));
    	}
    	if(i<=4*rows_s-4) {
    	s.addElement(new S(lists.get(i), lists.get(i+1), lists.get(i+2), lists.get(i+3)));
    	}
    	if(i<=4*rows_q-4) {
    	q.addElement(new Q(listq.get(i), listq.get(i+1), listq.get(i+2), listq.get(i+3)));
    	}

    }
    
    
    boolean status = OK;
    int numS = s.size(); // number of entry in s
    int numS_attrs = 4;
    int numR = r.size(); // number of entry in r
    int numR_attrs = 4;
    int numQ = q.size(); // number of entry in s
    int numQ_attrs = 4;

    String dbpath = "/tmp/" + System.getProperty("user.name") + ".minibase.jointest2db";
    String logpath = "/tmp/" + System.getProperty("user.name") + ".joinlog2";

    String remove_cmd = "/bin/rm -rf ";
    String remove_logcmd = remove_cmd + logpath;
    String remove_dbcmd = remove_cmd + dbpath;
    String remove_joincmd = remove_cmd + dbpath;

    try {
      Runtime.getRuntime().exec(remove_logcmd);
      Runtime.getRuntime().exec(remove_dbcmd);
      Runtime.getRuntime().exec(remove_joincmd);
    } catch (IOException e) {
      System.err.println("" + e);
    }

    SystemDefs sysdef = new SystemDefs(dbpath, 30000, NUMBUF, "Clock");

/////////////////////////////////////////////////////////////////////////////////////////////////
////								Create S database										/////
/////////////////////////////////////////////////////////////////////////////////////////////////
    
    // creating the S relation
    AttrType[] Stypes = new AttrType[4];
    Stypes[0] = new AttrType(AttrType.attrInteger);
    Stypes[1] = new AttrType(AttrType.attrInteger);
    Stypes[2] = new AttrType(AttrType.attrInteger);
    Stypes[3] = new AttrType(AttrType.attrInteger);

    // SOS
    short[] Ssizes = new short[1];
    Ssizes[0] = 0;

    Tuple t = new Tuple();
    try {
    	
      t.setHdr((short) 4, Stypes, Ssizes);
    } 
    catch (Exception e) {
    	
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    
    }

    int size = t.size();

    // inserting the tuple into file "S"
    RID rid;
    Heapfile f = null;
    
    try {
    	
      f = new Heapfile("S.in");
      
    } catch (Exception e) {
    	
      System.err.println("*** error in Heapfile constructor ***");
      status = FAIL;
      e.printStackTrace();
      
    }

    t = new Tuple(size);
    try {
      t.setHdr((short) 4, Stypes, Ssizes);
    } catch (Exception e) {
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    }

    for (int i = 0; i < numS; i++) {
      try {
        t.setIntFld(1, ((S) s.elementAt(i)).int_1);
        t.setIntFld(2, ((S) s.elementAt(i)).int_2);
        t.setIntFld(3, ((S) s.elementAt(i)).int_3);
        t.setIntFld(4, ((S) s.elementAt(i)).int_4);
      } catch (Exception e) {
        System.err.println("*** Heapfile error in Tuple.setStrFld() ***");
        status = FAIL;
        e.printStackTrace();
      }

      try {
        rid = f.insertRecord(t.returnTupleByteArray());
      } catch (Exception e) {
        System.err.println("*** error in Heapfile.insertRecord() ***");
        status = FAIL;
        e.printStackTrace();
      }
    }
    if (status != OK) {
      // bail out
      System.err.println("*** Error creating relation for S");
      Runtime.getRuntime().exit(1);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
    

/////////////////////////////////////////////////////////////////////////////////////////////////
////							Create R database											/////
/////////////////////////////////////////////////////////////////////////////////////////////////
    // creating the R relation
    
    AttrType[] Rtypes = new AttrType[4];
    Rtypes[0] = new AttrType(AttrType.attrInteger);
    Rtypes[1] = new AttrType(AttrType.attrInteger);
    Rtypes[2] = new AttrType(AttrType.attrInteger);
    Rtypes[3] = new AttrType(AttrType.attrInteger);

    short[] Rsizes = new short[1];
    Rsizes[0] = 0;
    
    t = new Tuple();
    try {
    	
      t.setHdr((short) 4, Rtypes, Rsizes);
    } catch (Exception e) {
    	
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    }

    size = t.size();

    // inserting the tuple into file "R"
    f = null;
    try {
    	
      f = new Heapfile("R.in");
    } catch (Exception e) {
      System.err.println("*** error in Heapfile constructor ***");
      status = FAIL;
      e.printStackTrace();
    }

    t = new Tuple(size);
    try {
      t.setHdr((short) 4, Rtypes, Rsizes);
    } catch (Exception e) {
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    }

    for (int i = 0; i < numR; i++) {
      try {
        t.setIntFld(1, ((R) r.elementAt(i)).int_1);
        t.setIntFld(2, ((R) r.elementAt(i)).int_2);
        t.setIntFld(3, ((R) r.elementAt(i)).int_3);
        t.setIntFld(4, ((R) r.elementAt(i)).int_4);

      } catch (Exception e) {
        System.err.println("*** error in Tuple.setStrFld() ***");
        status = FAIL;
        e.printStackTrace();
      }

      try {
        rid = f.insertRecord(t.returnTupleByteArray());
      } catch (Exception e) {
        System.err.println("*** error in Heapfile.insertRecord() ***");
        status = FAIL;
        e.printStackTrace();
      }
    }
    if (status != OK) {
      // bail out
      System.err.println("*** Error creating relation for R");
      Runtime.getRuntime().exit(1);
    }
/////////////////////////////////////////////////////////////////////////////////////////////////
    
    
/////////////////////////////////////////////////////////////////////////////////////////////////
////								Create Q database										/////
/////////////////////////////////////////////////////////////////////////////////////////////////
    // creating the Q relation
    AttrType[] Qtypes = new AttrType[4];
    Qtypes[0] = new AttrType(AttrType.attrInteger);
    Qtypes[1] = new AttrType(AttrType.attrInteger);
    Qtypes[2] = new AttrType(AttrType.attrInteger);
    Qtypes[3] = new AttrType(AttrType.attrInteger);

    short[] Qsizes = new short[1];
    Qsizes[0] = 0;
    t = new Tuple();
    try {
      t.setHdr((short) 4, Qtypes, Qsizes);
    } catch (Exception e) {
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    }

    size = t.size();

    // inserting the tuple into file "Q"
    f = null;
    try {
      f = new Heapfile("Q.in");
    } catch (Exception e) {
      System.err.println("*** error in Heapfile constructor ***");
      status = FAIL;
      e.printStackTrace();
    }

    t = new Tuple(size);
    try {
      t.setHdr((short) 4, Qtypes, Qsizes);
    } catch (Exception e) {
      System.err.println("*** error in Tuple.setHdr() ***");
      status = FAIL;
      e.printStackTrace();
    }

    for (int i = 0; i < numQ; i++) {
      try {
        t.setIntFld(1, ((Q) q.elementAt(i)).int_1);
        t.setIntFld(2, ((Q) q.elementAt(i)).int_2);
        t.setIntFld(3, ((Q) q.elementAt(i)).int_3);
        t.setIntFld(4, ((Q) q.elementAt(i)).int_4);

      } catch (Exception e) {
        System.err.println("*** error in Tuple.setStrFld() ***");
        status = FAIL;
        e.printStackTrace();
      }

      try {
        rid = f.insertRecord(t.returnTupleByteArray());
      } catch (Exception e) {
        System.err.println("*** error in Heapfile.insertRecord() ***");
        status = FAIL;
        e.printStackTrace();
      }
    }
    if (status != OK) {
      // bail out
      System.err.println("*** Error creating relation for Q");
      Runtime.getRuntime().exit(1);
    }
///////////////////////////////////////////////////////////////////////////////////////////
  }


  
  
  
  public static void main(String argv[]) throws JoinsException, IndexException, InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception
	{
		
		// max rows = 400
		JoinsDriver_ jjoin = new JoinsDriver_(400);

		System.out.println(""
				+ "**************************************************\n"
				+ "   Query_1a executed using NestedLoopsJoins       \n"
				+ " 	Using Class Constructed for task 1a          \n"
				+ "***************************************************");
		QueryTrait1 trait_1a = new QueryTrait1("QueriesData/query_1a.txt","NLJ","query_1a");
		trait_1a.execute_query();
		System.out.println(""
				+ "*****************************************\n"
				+ "Query_1a with NLJ Succesfully\n"
				+ "*****************************************");
		System.out.println(""
				+ "**************************************************\n"
				+ "Query_1b executed using NestedLoopsJoins       \n"
				+ "Using Class Constructed for task 1b          \n"
				+ "***************************************************");
		QueryTrait1 trait_1b = new QueryTrait1("QueriesData/query_1b.txt","NLJ","query_1b");
		trait_1b.execute_query();
		System.out.println(""
				+ "*****************************************\n"
				+ "Query_1b with NLJ Succesfully\n"
				+ "*****************************************");
		System.out.println(""
				+ "**************************************************\n"
				+ "Query_2a executed using IESelfJoinOneCond      \n"
				+ "					    				\n"
				+ "Using Class Constructed for task 2a          \n"
				+ "***************************************************");
		QueryTrait1 trait1_2a = new QueryTrait1("QueriesData/query_2a.txt","IESelfJoinOneCond","query_1a");
		trait1_2a.execute_query();
		System.out.println(""
				+ "*****************************************\n"
				+ "Query 2a with IESelfJoinOneCond Succesfully\n"
				+ "*****************************************");
		QueryTrait2.execute("QueriesData/query_2b.txt", 400, "IESelfJoinTwoPred");
		QueryTrait2.execute("QueriesData/query_2b.txt", 400, "NLJ");
		QueryTrait2.execute("QueriesData/query_2b.txt", 400, "JoinSelf");
		QueryTrait2.execute("QueriesData/query_2c.txt", 400, "IESelfJoinTwoPred");
		QueryTrait2.execute("QueriesData/query_2c.txt", 400, "NLJ");
		QueryTrait2.execute("QueriesData/query_2c.txt", 400, "JoinSelf");
		
	}
  
}
