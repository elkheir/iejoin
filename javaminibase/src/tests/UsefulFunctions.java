package tests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Vector;

import heap.Tuple;
import iterator.Iterator;

public class UsefulFunctions {
	
	private int counter_rows;

	public UsefulFunctions() {
		super();
	}
	
	
	
	public ArrayList<Integer> tighten_rows(String filename,int rows) {
		 
		 ArrayList<Integer> table = new ArrayList<Integer>();	
		 BufferedReader bfreader;
		
		 Integer counter = 0;
		 try {
		     bfreader = new BufferedReader(
		    		 						new FileReader("QueriesData/"+filename)
		    		 						
		    		 );
		     
		     String line = bfreader.readLine();
		     if (line != null) {
		        line = bfreader.readLine(); // eliminate the headers
		     }
		     
		     while (line != null && counter < rows) {
		     counter += 1;
		        
		     String[] Attrs = line.trim().split(",");

		     table.add(Integer.parseInt(Attrs[0]));
		     table.add(Integer.parseInt(Attrs[1]));
		     table.add(Integer.parseInt(Attrs[2]));
		     table.add(Integer.parseInt(Attrs[3]));
		        

		     line = bfreader.readLine();
		     }
		     this.counter_rows = counter;
		     bfreader.close();
		    
		 } catch (IOException e) {
		  
			 e.printStackTrace();
		    
		 }
		
		
		return table;
		
	}
	
	public int getNumRows() {
		
		return counter_rows;
	
	}
	public void write_to_file(String query, Iterator T) throws FileNotFoundException {
		
		Tuple t;
		t = null;
		int i = 0;
		
		PrintWriter pw = new PrintWriter("Outputs/output_" + query + ".txt");
		try {
		
			
			while ((t = T.get_next()) != null) {
				
				i++;
				pw.print(t.getIntFld(1)+","+t.getIntFld(2)+"\n"); 
			
			}
			pw.close();
			System.out.println("Output Tuples for " + query + ": " + i);

			
		} catch (Exception e) {
			System.err.println("" + e);
			e.printStackTrace();
			Runtime.getRuntime().exit(1);
		}
		
	}
	
	
}
