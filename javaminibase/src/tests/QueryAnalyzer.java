package tests;

import iterator.*;
	import heap.*;
	import global.*;
	import index.*;
	import java.io.*;
	import java.util.*;
	import java.lang.*;
	import diskmgr.*;
	import bufmgr.*;
	import btree.*; 
	import catalog.*;
	
public class QueryAnalyzer {
	
		private String pathtoquery;  
		private CondExpr[] outFilter;
		private Integer[] column_selected;
	    private String[] relations;        // innerRelation, outerRelation
		
		private String[] operations = {"=", "<", ">", "!=", "<=", ">="}; // as defined 
	    
		public QueryAnalyzer(String pathtoquery) {
			this.pathtoquery=pathtoquery;
		    this.outFilter = new CondExpr[3];
		    this.column_selected = new Integer[2];
		    this.relations = new String[2]; 
			
		    execute();
		}


		private void execute() {
			
			/*
			 *  LINE_1: Rel1 col# Rel2 col#
			 *  LINE_2: Rel1 Rel2
		     *  LINE_3: Rel1 col# op 1 Rel2 col#
		     *  LINE_4: AND/OR
		     *  LINE_5: Rel1 col# op 2 Rel2 col#
		     */  
		  
			
			/*
			 *    SELECT  selectRel1 <selectRel1Col> selectRel2 <selectRel2Col>
			 * 	  FROM relation1 relation2 
			 * 	  WHERE Where_rel11 <rel1_col1> "operation1"  Where_rel21 <rel2_col1>
			 * 	  interJoin 
			 *    WHERE rel12 <rel1_col2>  "operation2" rel22 <rel2_col2>
			 */
			
			// variables Line 1
			String selectRel1 = "", selectRel2 = "";
		    int selectRel1Col = 0, selectRel2Col = 0;
		    
		    // variables Line 2
		    String rel1 = "", rel2 = "";
		    
		    // variables Line 3 ...
		    String rel11 = "", rel21 = "", rel12 = "", rel22 = "";
		    int rel1_col1 = 0, rel2_col1 = 0, rel1_col2 = 0, rel2_col2 = 0;
		    
		    // variables op1 op2
		    int operation1 = 0, operation2 = 0;
		    String interJoin = "";
		   
		    // let's assume initially ther's one predicate 
		    Boolean twopredicates = false;
		    
		    try {
		    
		    	  FileReader file_query = new FileReader(this.pathtoquery); 
			      BufferedReader query = new BufferedReader(file_query);
			      
			      // Line1
			      
			      String[] line1 = query.readLine().split(" ");
			      
			      selectRel1 = line1[0].split("_")[0]; // rel1
			      selectRel2 = line1[1].split("_")[0]; // rel2
			      
			      selectRel1Col = Integer.parseInt(line1[0].split("_")[1]);
			      selectRel2Col = Integer.parseInt(line1[1].split("_")[1]);
			      
			      // Line2
			      
			      String[] line2 = query.readLine().split(" ");
			      
			      rel1 = line2[0];
			      if (line2.length < 2) {
			        rel2 = rel1;
			      } else {
			        rel2 = line2[1];
			      }
			      
			      // Line3: 1st predicate
			      
			      String[] line3 = query.readLine().split(" ");
			      operation1 = Integer.parseInt(line3[1]);
			      
			      rel11 = line3[0].split("_")[0];
			      rel21 = line3[2].split("_")[0];
			      
			      rel1_col1 = Integer.parseInt(line3[0].split("_")[1]);
			      rel2_col1 = Integer.parseInt(line3[2].split("_")[1]);
			      
			      // Line4
			      
			      String line4 = query.readLine();
			      if (line4 != null) {
			    	  
			        twopredicates = true;
			        interJoin = line4;
			        
			        // Line5: 2nd predicate
			        String[] line5 = query.readLine().split(" ");
			        operation2 = Integer.parseInt(line5[1]);
			        
			        rel12 = line5[0].split("_")[0];
			        rel22 = line5[2].split("_")[0];
			        
			        rel1_col2 = Integer.parseInt(line5[0].split("_")[1]);
			        rel2_col2 = Integer.parseInt(line5[2].split("_")[1]);
			      }
			      query.close();
			    } catch (FileNotFoundException ex) {
			      ex.printStackTrace();
			    } catch (IOException ex) {
			      ex.printStackTrace();
			    }

			    // CondExpr
		    
			    outFilter[0] = new CondExpr();
			    outFilter[0].next = null;
			    outFilter[0].op = new AttrOperator(operation1);
			    outFilter[0].type1 = new AttrType(AttrType.attrSymbol);
			    outFilter[0].type2 = new AttrType(AttrType.attrSymbol);
			    outFilter[0].operand1.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), rel1_col1);
			    outFilter[0].operand2.symbol = new FldSpec(new RelSpec(RelSpec.outer), rel2_col1);

			    if (twopredicates) {
			    	outFilter[1] = new CondExpr();
			    	outFilter[1].next = null;
			    	outFilter[1].op = new AttrOperator(operation2);
			    	outFilter[1].type1 = new AttrType(AttrType.attrSymbol);
			    	outFilter[1].type2 = new AttrType(AttrType.attrSymbol);
			    	outFilter[1].operand1.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), rel1_col2);
			    	outFilter[1].operand2.symbol = new FldSpec(new RelSpec(RelSpec.outer), rel2_col2);
			    	outFilter[2] = null;
			    } else {
			    	outFilter[1] = null;
			    }

			    column_selected[0] = selectRel1Col;
			    column_selected[1] = selectRel2Col;
			    relations[0] = new String(rel1); // inner
			    relations[1] = new String(rel2); // outer
			
		}
	
	
		public CondExpr[] getOutFilter() {
			return outFilter;
		}

		public void setOutFilter(CondExpr[] outFilter) {
			this.outFilter = outFilter;
		}

		public Integer[] getColumn_selected() {
			return column_selected;
		}

		public void setColumn_selected(Integer[] column_selected) {
			this.column_selected = column_selected;
		}

		public String[] getRelations() {
			return relations;
		}

		public void setRelations(String[] relations) {
			this.relations = relations;
		}
}
