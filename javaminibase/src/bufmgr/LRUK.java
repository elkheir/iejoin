package bufmgr;

public class LRUK extends Replacer {

	/*
	 * Used after but initialized to 0 
	 */
	
	private static final long Correlated_Reference_Period = 0;

	/*
	 * 
	 * hist[frameNo][index] 
	 * Refers to index_th access (in term of time) of frameNo
	 */
	
	private long hist[][];

	/*
	 * last[frameNo]
	 * the recent reference to frameNo
	 * i terms of time
	 */
	
	private long last[];
	
	
	/*
	 * number of references to program within it 
	 */
	
	private int numRef;

	/*
	 * As usual 
	 * An array to hold number of frames in the buffer pool
	 */
	
	private int frames[];

	/*
	 * number of frames used
	 */
	private int nFrames;



	
	
	/*
	 * ################################ Constructor #################################
	 */
	
	/*
	 * Initializing frames[] pinter = null.
	 */
	public LRUK(BufMgr mgrArg)
	{
		super(mgrArg);
		frames = null;
		
		this.numRef = mgr.getRefNum()  ;
	}
	/*
	 * ################################ Set Buffer Manager ############################
	 * 
	 */
	
	/* 
	 * nFrame used to zero
	 * List of Frames used initialized [len(buffer number)]
	 * Initialize hist (Dimension of Frames*number_of_references)
	 * Initialize last (Dimension Frames -> last_access)
	 * @param	mgr	a BufMgr object
	 * @see	BufMgr
	 * @see	Replacer
	 */
	
	public void setBufferManager( BufMgr mgr )
	{
		super.setBufferManager(mgr);
		nFrames = 0;
		frames = new int [mgr.getNumBuffers()];
		hist = new long [mgr.getNumBuffers()][numRef];
		last = new long [mgr.getNumBuffers()];
	}

	
	/*
	 * ################### ALGORITHM CODING #####################################
	 */
	
	/**
	 * Do updates according to this algorithm
	 * See below explanations 
	 * @param frameNo	the frame number
	 */
	
	private void update(int frameNo){
		              
		boolean	in_frame_list = false; // = (This page "FRameNo" is it already in our buffer ?)
		long 	t = System.currentTimeMillis(); // (time)
		
		
		for ( int index=0; index < nFrames; ++index )
			if ( frames[index] == frameNo ) {
				in_frame_list = true;
				break;
			}

		// distinguish cases 
		
		/*
		 * ############FRameNo" is it already in our buffer######################
		 */
		       /*
		        *  in this case there's two cases  : 
		        *  the first : if previous_last_time_access and time_access are correlated
		        *  ==> update hist and last
		        *  the second : ifnot previous_last_time_access and time_access aren't correlated 
		        */
				

		
		if (in_frame_list) {
			if ( t-last[frameNo]>= Correlated_Reference_Period ) {
				
				// the first case
				long refrence_page = last[frameNo] - hist[frameNo][0];
				for (int i=1; i<numRef; i++) {
					hist[frameNo][i] = hist[frameNo][i-1] + refrence_page;
				}
				hist[frameNo][0] = t;
				last[frameNo] = t;
			} else {
				// the second case
				last[frameNo] = t;
			}

		} 
	}
	
	/*
	 * ##############################################################################"
	 * Pick a victim using LRUK policy 
	 * 
	 * @return 	return the frame number
	 *	     	return BufferPoolExceededException if failed
	 */
	
	public int pick_victim() throws BufferPoolExceededException
	{
		int numBuffers = mgr.getNumBuffers();
		int frame = -1; // there's no frame (At the beginning)
		long t = System.currentTimeMillis();
		
		
		// min of last access time of all frames is initialized to "t"
		long min = t;
		int victim = -1;
		
		
		// if buffer not full yet
		// "we're lucky" jut take another frame
		
		if ( nFrames < numBuffers ) {
			frame = nFrames++;
			frames[frame] = frame;
			state_bit[frame].state = Pinned;
			(mgr.frameTable())[frame].pin();
			return frame;
		}
		
		
		// if all frames used 
		// SO here's the algorithm of LRUK
		// we pick this frame (Victim Frame)
		// thats coincides with max backward distance as seen in lectures  
		// maximum backward K-distance
		
		// we're looking for min now and then reference the victim to the covenant frame 
		for ( int i = 0; i < numBuffers; ++i ) {
			frame = frames[i];
			if ( t-last[frame]>= Correlated_Reference_Period && hist[frame][numRef-1]<=min && state_bit[frame].state != Pinned ) {
				victim = frame;
				min = hist[victim][numRef-1];
			}
		}
		
		
		if (victim >=0) {
			state_bit[victim].state = Pinned;
			(mgr.frameTable())[victim].pin();
			return victim;
		}

		throw new BufferPoolExceededException (null, "BUFMGR: BUFMGR_EXCEEDED.");
	}

	
	
	/*
	 * call super class the same method
	 * pin the page in the given frame number 
	 * and update (page)
	 *
	 * @param	 frameNo	 the frame number to pin
	 * @exception  InvalidFrameNumberException
	 */
	
	public void pin(int frameNo) throws InvalidFrameNumberException
	{
		super.pin(frameNo);
		update(frameNo);
	}

	 
	//########################################################################
	
	public String name() { return "LRUK"; }

	/*
	 * print out the information of frame usage
	 */  
	public void info()
	{
		super.info();
		System.out.print( "LRU-k REPLACEMENT");

		for (int i = 0; i < nFrames; i++) {
			if (i % 5 == 0)
				System.out.println( );
			System.out.print( "\t" + frames[i]);
		}
		System.out.println();
	} 

	
	// Used in test4()
	public int[] getFrames() { return frames;	}

	
	// Used in test4()
	// This methods returns the i_th older access of our pagenumber 
	public long HIST(int pagenumber, int i) { return hist[pagenumber][i];	}

	
	// returns last access (time) of pagenumber
	public long LAST(int pagenumber) {
		return last[pagenumber];
	}

} 