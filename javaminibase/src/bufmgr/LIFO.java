

/* File LIFO.java */

package bufmgr;

import java.util.ArrayList;

import diskmgr.*;
import global.*;

  /**
   * class LIFO is a subclass of class Replacer using LIFO
   * algorithm for page replacement
   */

class LIFO extends Replacer {

/* private fields and methods */

  /**
   * private field
   * 
   *q_Frames : An arraylist to registrate Frame  in the buffer pool
   * 
   * nFrames number of frame in buffer
   */
	
 private ArrayList<Integer> q_Frames;


 private int nFrames;
 
 
/**
 *
 * Calling super class the same method
 * Initializing the q_Frames[] with number of buffer allocated
 * 
 * set each element of q_Frames to -1 (not allocated yet)
 *
 * @param     mgr      Buffer manager
 * @see       BufMgr
 * @see       Replacer
 */

 public void setBufferManager( BufMgr mgr )
 {
     super.setBufferManager(mgr);

    this.nFrames = mgr.getNumBuffers();
    q_Frames = new ArrayList<Integer>();

    for ( int index = 0; index < nFrames; ++index ) {
        q_Frames.add(index,-1);
    }
}

 
 
/**
 * Constructor 
 * Initializing q_Frames[] = null.
 */

 
public  LIFO(BufMgr mgrArg)
{
   super(mgrArg);
   q_Frames = null;
   nFrames = mgrArg.getNumBuffers();

}


/**
 * Adding the frame with given frame number to buffer pool
 * Add it to the beginning of the list front of the list element [0] 'like a queue' LIFO
 *
 * @param	frameNo	 the frame number
 * @see 	BufMgr
 */

 private void update(int frameNo)
 {
     int index;
     int numBuffers=mgr.getNumBuffers();
     int state = 0;
     for ( index=0; index < numBuffers; ++index ) {
    	 
         if ( q_Frames.get(index) == -1  ||  q_Frames.get(index) == frameNo ) 
        	 break;
        
         
     }

    // If buffer pool is not full, we add this frame to our list  (We're lucky this time)
    if ( q_Frames.get(index) < 0 ) {
        q_Frames.set(index,frameNo);
    }
    
    int frame = q_Frames.get(index);
    
     /*
     *move all the q_Frames downwards to fill the place of the freed frame
     *place the freed frame on the top of the stack
     */
    while ( index-- >0) {
     
      q_Frames.set(index+1, q_Frames.get(index));
      //index = index-1;
     
    } 
    q_Frames.set(0,frame);
   
 }

 
 
/**
 * pin the page with the given frame number
 * "update the buffer pool" will be included in pick_victim method 
 *
 * @param       frameNo  the frame number to pin
 * @@exception  InvalidFrameNumberException
 */

 
public void pin(int frameNo) throws InvalidFrameNumberException
{
    super.pin(frameNo);
}


  /**
   * 
   * there's to two situations : 
   * 	1 - Buffer's Frame aren't yet full 
   * 		==> we're lucky here 
   * 		==> just update(q_Frame)
   *    2 - Choose a page following LIFO POLICY
   *        ==> update(q_Frame)
   * @return    return the frame number
   *            throws BufferPoolExceededException if No victims found instead of -1
   */


public int pick_victim() throws BufferPoolExceededException
{
   int numBuffers = mgr.getNumBuffers();
   int frame;

    for (int i=0; i < numBuffers; ++i) {
           if (q_Frames.get(i) ==-1) { /*
        							*free frame as we set before "q_Frames" 
        							*will be -1 for unoccupied frames     
        							*
        						    */         
            q_Frames.set(i, -i *q_Frames.get(i));
            frame = q_Frames.get(i);
            state_bit[frame].state = Pinned;
            (mgr.frameTable())[frame].pin();
            
            //update queue (this frame will be at the top of the queue [0])
            update(frame);
            
            // break;
            return frame; 
            
        }
    }
    /*
     *the buffer is full 'Hell no'
     *we pick the first unpinned page in the buffer pool
     */

    for (int i = 0; i < numBuffers; ++i) {
         frame = q_Frames.get(i);
         
         // if this frame is not pinned by the frame
         
         if ( state_bit[frame].state != Pinned ) {
            state_bit[frame].state = Pinned;
            (mgr.frameTable())[frame].pin();
            
            /*
             *we update the q_Frames 
             *by moving the frame of the victim page
             *and placing to the top "position 0"
             */
            
            update(frame);
            
            //break
            
            return frame;
        }
       }

      // if no frame was picked,so the buffer is full and all of frames are pinned 
      //return -1;
	  throw new BufferPoolExceededException (null, "BUFMGR: BUFFER_EXCEEDED.");

}

/**
 * get the page replacement policy name
 *
 * @return    return the name of replacement policy used
 */

   public String name() { return "LIFO"; }

/**
 * print out the information of frame usage
 */

 public  void info()
 {
    super.info();

    System.out.print( "LIFO REPLACEMENT");

    for (int i = 0; i < mgr.getNumBuffers(); i++) {
        if (i % 5 == 0)
	System.out.println();
	System.out.print( "\t" + q_Frames.get(i));

    }
    System.out.println();
 }


}