/* File FIFO.java */

package bufmgr;

import java.util.ArrayList;

import diskmgr.*;
import global.*;

  /**
   * class FIFO is a subclass of class Replacer using FIFO
   * algorithm for page replacement
   */

class FIFO extends Replacer {

/* private fields and methods */

  /**
   * private field
   * 
   *q_Frames : An arraylist to registrate Frames status in the buffer pool
   * 
   * nFrames number of frame in buffer useful (page inside it)
   */
	
 private ArrayList<Integer> q_Frames;


 private int nFrames;
 
 
/**
 *
 * Calling super class the same method
 * Initializing the q_Frames[] with number of buffer allocated
 * 
 * set each element of q_Frames to -1 (not allocated yet)
 *
 * @param     mgr      Buffer manager
 * @see       BufMgr
 * @see       Replacer
 */

 
 public void setBufferManager( BufMgr mgr )
 {
     super.setBufferManager(mgr);

    this.nFrames = 0;
    q_Frames = new ArrayList<Integer>();

    for ( int index = 0; index < mgr.getNumBuffers(); ++index ) {
        q_Frames.add(index,-1);
    }
}

 
 
/**
 * Constructor 
 * Initializing q_Frames[] = null.
 */

 
public  FIFO(BufMgr mgrArg)
{
   super(mgrArg);
   q_Frames = null;
   //nFrames = mgrArg.getNumBuffers();

}


/**
 * Adding the frame with given frame number to buffer pool
 * Add it to the end of the list [nFrames -1] 'like a queue' FIFO
 *
 * @param	frameNo	 the frame number
 * @see 	BufMgr
 */

 private void update(int frameNo)
 {
     int index;
     int numBuffers=mgr.getNumBuffers();
     
     for ( index=0; index < nFrames; ++index ) {
    	 /*
    	  * if this frameÀ presented in one of our frame in q_frames 
    	  */
         if (q_Frames.get(index) == frameNo ) 
        	 break;
          
     }


    while ( ++index < nFrames) {
     
      q_Frames.set(index-1, q_Frames.get(index));
      
      q_Frames.set(nFrames -1,frameNo);
      
    } 
    
 }

 
 
/**
 * pin the page with the given frame number
 * "update the buffer pool" will be included in pick_victim method 
 *
 * @param       frameNo  the frame number to pin
 * @@exception  InvalidFrameNumberException
 */

 
public void pin(int frameNo) throws InvalidFrameNumberException
{
    super.pin(frameNo);
}


  /**
   * 
   * there's to two situations : 
   * 	1 - Buffer's Frame aren't yet full 
   * 		==> we're lucky here 
   * 		==> just update(q_Frame)
   *    2 - Choose a page following FIFO POLICY
   *        ==> update(q_Frame)
   * @return    return the frame number
   *            throws BufferPoolExceededException if No victims found instead of -1
   */


public int pick_victim() throws BufferPoolExceededException
{
   int numBuffers = mgr.getNumBuffers();
   int frame;

   // as LRU algorithm 
   if ( nFrames < numBuffers ) {
   /*
    *the buffer is not full
    *we add the page to the end of the buffer pool
    */
       frame = nFrames++;
       q_Frames.set(frame,frame);
       state_bit[frame].state = Pinned;
       (mgr.frameTable())[frame].pin();
       
       // break
       
       /*
        * return the num of frame
        */
       return frame;
   }
    /*
     *the buffer is full 'Hell no'
     *we pick the last unpinned page in the buffer pool
     */

    for (int i = 0; i < numBuffers; ++i) {
         frame = q_Frames.get(i);
         
         // if this frame is not pinned by the frame
         
         if ( state_bit[frame].state != Pinned ) {
            state_bit[frame].state = Pinned;
            (mgr.frameTable())[frame].pin();
            
            /*
             *we update the q_Frames 
             *by moving the frame of the victim page
             *and placing to the top "position nFRames-1 = 
             *  number of frames in the buffer -1"
             */
            
            update(frame);
            
            //break
            
            return frame;
        }
       }

      // if no frame was picked,so the buffer is full and all of frames are pinned 
      //return -1;
	  throw new BufferPoolExceededException (null, "BUFMGR: BUFFER_EXCEEDED.");

}

/**
 * get the page replacement policy name
 *
 * @return    return the name of replacement policy used
 */

   public String name() { return "FIFO"; }

/**
 * print out the information of frame usage
 */

 public  void info()
 {
    super.info();

    System.out.print( "FIFO REPLACEMENT");

    for (int i = 0; i < mgr.getNumBuffers(); i++) {
        if (i % 5 == 0)
	System.out.println();
	System.out.print( "\t" + q_Frames.get(i));

    }
    System.out.println();
 }


}
