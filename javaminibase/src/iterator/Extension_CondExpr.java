package iterator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import global.AttrOperator;
import global.AttrType;

public class Extension_CondExpr {
	
	private CondExpr[] outFilter;
	private Integer[] column_selected;
	private String[] relations;
	
	
	
	public Extension_CondExpr(CondExpr[] outFilter,Integer[] column_selected,String[] relations) {
		this.outFilter = outFilter;
		this.column_selected = column_selected;
		this.relations = relations;
	}
	
	public void apply_condexpr_query(String query_path) {
	    // LINE 1: Rel1 col# Rel2 col#
	    // LINE 2: Rel1 Rel2
	    // LINE 3: Rel1 col# op 1 Rel2 col#
	    // LINE 4: AND/OR
	    // LINE 5: Rel1 col# op 2 Rel2 col#
	    String selectRel1 = "", selectRel2 = "";
	    Integer selectRel1Col = 0, selectRel2Col = 0;
	    String rel1 = "", rel2 = "";
	    String whereRel1_1 = "", whereRel2_1 = "", whereRel1_2 = "", whereRel2_2 = "";
	    Integer whereRel1Col_1 = 0, whereRel2Col_1 = 0, whereRel1Col_2 = 0, whereRel2Col_2 = 0;
	    Integer op1 = 0, op2 = 0;
	    String interPredicate = "";
	    String[] op_string = {"=", "<", ">", "!=", "<=", ">="};
	    Boolean doublePredicate = false;
	    
	    try {
	      BufferedReader query = new BufferedReader(new FileReader(query_path));
	      
	      // Line1
	      String[] line1 = query.readLine().split(" ");
	      selectRel1 = line1[0].split("_")[0];
	      selectRel2 = line1[1].split("_")[0];
	      selectRel1Col = Integer.parseInt(line1[0].split("_")[1]);
	      selectRel2Col = Integer.parseInt(line1[1].split("_")[1]);
	      
	      // Line2
	      String[] line2 = query.readLine().split(" ");
	      rel1 = line2[0];
	      if (line2.length < 2) {
	        rel2 = rel1;
	      } else {
	        rel2 = line2[1];
	      }
	      
	      // Line3: 1st predicate
	      String[] line3 = query.readLine().split(" ");
	      op1 = Integer.parseInt(line3[1]);
	      whereRel1_1 = line3[0].split("_")[0];
	      whereRel2_1 = line3[2].split("_")[0];
	      whereRel1Col_1 = Integer.parseInt(line3[0].split("_")[1]);
	      whereRel2Col_1 = Integer.parseInt(line3[2].split("_")[1]);
	      
	      // Line4
	      String line4 = query.readLine();
	      if (line4 != null) {
	        doublePredicate = true;
	        interPredicate = line4;
	        // Line5: 2nd predicate
	        String[] line5 = query.readLine().split(" ");
	        op2 = Integer.parseInt(line5[1]);
	        whereRel1_2 = line5[0].split("_")[0];
	        whereRel2_2 = line5[2].split("_")[0];
	        whereRel1Col_2 = Integer.parseInt(line5[0].split("_")[1]);
	        whereRel2Col_2 = Integer.parseInt(line5[2].split("_")[1]);
	      }
	      query.close();
	    } catch (FileNotFoundException ex) {
	      ex.printStackTrace();
	    } catch (IOException ex) {
	      ex.printStackTrace();
	    }

	    System.out.print("  SELECT   " + selectRel1 + "." + selectRel1Col + " " + selectRel2 + "."
	        + selectRel2Col + "\n" + "  FROM     " + rel1 + " " + rel2 + "\n" + "  WHERE    "
	        + whereRel1_1 + "." + whereRel1Col_1 + " " + op_string[op1] + " " + whereRel2_1 + "."
	        + whereRel2Col_1 + " ");
	    if (doublePredicate) {
	      System.out.print(interPredicate + " " + whereRel1_2 + "." + whereRel1Col_2 + " "
	          + op_string[op2] + " " + whereRel2_2 + "." + whereRel2Col_2);
	    }
	    System.out.print("\nPlan used:\n" + "  Pi(" + selectRel1 + "." + selectRel1Col + ", "
	        + selectRel2 + "." + selectRel2Col + ") (" + rel1 + " |><| " + rel2 + "))\n\n");

	    ArrayList<CondExpr> expr = new ArrayList<CondExpr>();
		expr.add(0,new CondExpr());
	    expr.get(0).next = null;
	    expr.get(0).op = new AttrOperator(op1);
	    expr.get(0).type1 = new AttrType(AttrType.attrSymbol);
	    expr.get(0).type2 = new AttrType(AttrType.attrSymbol);
	    expr.get(0).operand1.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), whereRel1Col_1);
	    expr.get(0).operand2.symbol = new FldSpec(new RelSpec(RelSpec.outer), whereRel2Col_1);

	    if (doublePredicate) {
	    	expr.add(1,new CondExpr());
	    	expr.get(1).next = null;
	    	expr.get(1).op = new AttrOperator(op2);
	    	expr.get(1).type1 = new AttrType(AttrType.attrSymbol);
	    	expr.get(1).type2 = new AttrType(AttrType.attrSymbol);
	    	expr.get(1).operand1.symbol = new FldSpec(new RelSpec(RelSpec.innerRel), whereRel1Col_2);
	    	expr.get(1).operand2.symbol = new FldSpec(new RelSpec(RelSpec.outer), whereRel2Col_2);
	    	expr.add(2,null);
	    } else {
	    	expr.add(1,null);
	    }

	    this.column_selected[0] = selectRel1Col;
	    this.column_selected[1] = selectRel2Col;
	    this.relations[0] = new String(rel1); // inner
	    this.relations[1] = new String(rel2); // outer
	 
	}
	
	public CondExpr[] getOutFilter() {
		return outFilter;
	}
	public void setOutFilter(CondExpr[] outFilter) {
		this.outFilter = outFilter;
	}
	public Integer[] getColumn_selected() {
		return column_selected;
	}
	public void setColumn_selected(Integer[] column_selected) {
		this.column_selected = column_selected;
	}
	public String[] getRelations() {
		return relations;
	}
	public void setRelations(String[] relations) {
		this.relations = relations;
	}
	
}
