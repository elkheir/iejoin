package iterator;
import iterator.*;
import java.util.Stack;

import java.io.IOException;

import bufmgr.PageNotReadException;
import global.AttrOperator;
import global.AttrType;
import global.RID;
import global.TupleOrder;
import heap.FieldNumberOutOfBoundException;
import heap.Heapfile;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Scan;
import heap.Tuple;
import index.IndexException;
import java.util.ArrayList;
import java.util.ListIterator;

public class IESelfJoinOnePred extends Iterator {

	private AttrType      _in[];
	private   int        in_len;
	private   Iterator  outer;
	private   CondExpr OutputFilter[];
	private   CondExpr RightFilter[];
	private   int        n_buf_pgs;        // # of buffer pages available.
	private   boolean        done,         // Is the join complete
	get_from_outer;                 		// if TRUE, a tuple is got from outer
	private   Tuple     outer_tuple;
	private   Tuple     Jtuple;           // Joined tuple  
	private   FldSpec   perm_mat[];
	private   int        nOutFlds;
	private   Heapfile  hf;

	
	private Sort L;
	private ArrayList<Tuple> joinlist;
	
	public IESelfJoinOnePred( AttrType    in[],    
			int     len_in,           
			short   t1_str_sizes[],   
			int     amt_of_mem,        
			Iterator     am,
			CondExpr outFilter[],      
			CondExpr rightFilter[],    
			FldSpec   proj_list[],
			int        n_out_flds
			) throws SortException, LowMemException, JoinsException, Exception
	{
		
// Like the other Algorithms "inherited steps"
		_in = new AttrType[in.length];
		System.arraycopy(in,0,_in,0,in.length);
		in_len = len_in;

		outer = am;

		Jtuple = new Tuple();
		OutputFilter = outFilter;
		RightFilter  = rightFilter;

		n_buf_pgs    = amt_of_mem; // num pages for heapfile
		done  = false;
		get_from_outer = true;
		
		// Jtypes created for join projection of result tuples
		AttrType[] Jtypes = new AttrType[n_out_flds];

		short[] t_size;

		perm_mat = proj_list;
		nOutFlds = n_out_flds;
		try {
			t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes,
					in, len_in, in, len_in,
					t1_str_sizes, t1_str_sizes,
					proj_list, nOutFlds);
		}catch (TupleUtilsException e){
			throw new NestedLoopException(e, "TupleUtilsException is caught by IESelfJoins.java");
		}


// End of Extracted steps from other algorithms Method 
//---------------------------------------------------------------------------		
		// Sorting outer with sort and result in iterator
		L = null;
		sort_method(t1_str_sizes);
		// L array created
//---------------------------------------------------------------------------		
		// From a Sort to an ArrayList 
		Tuple tuple = new Tuple();
		tuple = null;
		ArrayList<Tuple> list = new ArrayList<Tuple>();
		while ((tuple = L.get_next()) != null)
		{	
			Tuple x = new Tuple(tuple); 
			list.add(x);
		}

		// Fill the joinList 
		this.joinlist = new ArrayList<Tuple>() ;	
//---------------------------------------------------------------------------		
		// Single predicate IESelfJoin
		//>> for every element of L1
		for (int i=0; i<list.size(); i++) {
		
			//>> for element L1 >0 >> i 
			
			for (int j=0; j <= i-1; j++) {
				
				//>> Join operation
				Tuple t1 = list.get(i);
				Tuple t2 = list.get(j);
				Projection.Join(t1, _in, 
								t2, _in, 
								Jtuple, perm_mat, nOutFlds);
				Tuple jtuple = new Tuple(Jtuple);
				joinlist.add(jtuple);

				}
			}
		}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------Methods Used
	// Backup Function for join for debugging
	public void join(Tuple T1, Tuple T2, Tuple jtuple) throws FieldNumberOutOfBoundException, IOException {
		
		//>> Join Tuples
		int element1 = T1.getIntFld(perm_mat[0].offset);
		int element2 = T1.getIntFld(perm_mat[1].offset);
		jtuple.setIntFld(1, element1);
		jtuple.setIntFld(2, element2);
		
	}
//---------------------------------------------------------------------------

	
	// close function to close scan of heapfile
	@Override
	public void close() throws IOException, JoinsException, SortException, IndexException {
		if (!closeFlag) {

			try {
				outer.close();
			}catch (Exception e) {
				throw new JoinsException(e, "NestedLoopsJoin.java: error in closing iterator.");
			}
			closeFlag = true;
		}
	}
//---------------------------------------------------------------------------
	
	@Override	
	public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
	InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
	LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {
		
		//>> take the first element and then drop it 
		//<< update automatically of the length 
		Tuple allowed = null;
		if (this.joinlist.size() > 0) {
			allowed = this.joinlist.get(0);
			this.joinlist.remove(0);
		}
		return allowed;
	} 
//---------------------------------------------------------------------------
	
	// sort L 
	public void sort_method(short[] t1_str_sizes) throws SortException, IOException, IteratorBMException {
		
		// check if sort asc or dsc
		
		// ascending
		
		if (OutputFilter[0].op.attrOperator == AttrOperator.aopGT || OutputFilter[0].op.attrOperator == AttrOperator.aopGE) {
			TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
			L = new Sort (_in, (short) in_len, t1_str_sizes,
					(iterator.Iterator) outer, OutputFilter[0].operand1.symbol.offset, ascending, t1_str_sizes[0], n_buf_pgs);
			
	
		// descending 
		} else if (OutputFilter[0].op.attrOperator == AttrOperator.aopLT ||OutputFilter[0].op.attrOperator == AttrOperator.aopLE)
		{
			TupleOrder descending = new TupleOrder(TupleOrder.Descending);
			L = new Sort (_in, (short) in_len, t1_str_sizes,
					(iterator.Iterator) outer, OutputFilter[0].operand1.symbol.offset, descending, t1_str_sizes[0], n_buf_pgs);
		}
		
	}
//---------------------------------------------------------------------------
}
