package iterator;

import java.io.IOException;
import java.util.ArrayList;

import bufmgr.PageNotReadException;
import global.AttrOperator;
import global.AttrType;
import global.TupleOrder;
import heap.FieldNumberOutOfBoundException;
import heap.Heapfile;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Tuple;
import index.IndexException;

public class JoinSelf extends Iterator {
	
	private AttrType in[]; 
	private AttrType in2[];
	private int len_in;
	private int len_in2;
    
	private Iterator outer; 
	private Iterator outer2;
	private Iterator inner; 
	private Iterator inner2;
	
	private short t1_str_sizes[];
	private short t2_str_sizes[];
	
	private CondExpr OutputFilter[];
	private CondExpr RightFilter[];
	
	private int n_buf_pgs;        // # of buffer pages available.
	private boolean done;      // Is the join complete
	
	private Tuple outer_tuple; 
	private Tuple inner_tuple;
	
	private   Tuple     Jtuple;           // Joined tuple
	private   FldSpec   perm_mat[];
	private   int        nOutFlds;
	private   Heapfile  hf;

	
	private Sort L1;
	private Sort L2;
	
	private Sort L1_prime;
	private Sort L2_prime;

	
	private ArrayList<Tuple> joinlist;
	
	public JoinSelf( AttrType    in1[],    
			int     lenin,           
			short   t1_str_sizes[],
			AttrType    in2[],         
			int     lenin2,           
			short   t2_str_sizes[],   
			int     amt_of_mem,        
			Iterator     outer, 
			Iterator     outer2,
			Iterator     inner, 
			Iterator     inner2,
			CondExpr outFilter[],      
			CondExpr rightFilter[],    
			FldSpec   proj_list[],
			int        n_out_flds
			) throws SortException, UnknowAttrType, LowMemException, JoinsException, Exception
	{

//---------------------------------------------------------------------------Inherited Steps
		in = new AttrType[in1.length];
		this.in2 = new AttrType[in2.length];
		
		System.arraycopy(in1,0,in,0,in1.length);
		System.arraycopy(in2,0,this.in2,0,in2.length);
		len_in = lenin;
		len_in2 = lenin2;

		this.t2_str_sizes =  t2_str_sizes;
		inner_tuple = new Tuple();
		Jtuple = new Tuple();
		OutputFilter = outFilter;
		RightFilter  = rightFilter;

		n_buf_pgs    = amt_of_mem;
		done  = false;

		perm_mat = proj_list;
		nOutFlds = n_out_flds;
		
		// Jtypes for Join projection
		AttrType[] Jtypes = new AttrType[n_out_flds];

		short[] t_size;

		perm_mat = proj_list;
		nOutFlds = n_out_flds;
		this.joinlist = new ArrayList<Tuple>();
		
		try {
			t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes,
					in1, lenin, in1, lenin,
					t1_str_sizes, t1_str_sizes,
					proj_list, nOutFlds);
		}catch (TupleUtilsException e){
			throw new NestedLoopException(e, "TupleUtilsException is caught by IESelfJoins.java");
		}

//---------------------------------------------------------------------------Inherited Steps
	
//---------------------------------------------------------------------------INITIALIZED
		// Initialize Sort List : L1, L2, L1_prime, L2_prime 
		L1 = sort_method_1(L1,in1,(short) len_in, outer, t1_str_sizes, 1);
		L2 = sort_method_2(L2,in1,(short) len_in,outer2,t1_str_sizes,1);
		L1_prime = sort_method_1(L1_prime, in2, (short) len_in2, inner, t2_str_sizes,2);
		L2_prime = sort_method_2(L2_prime, in2, (short) len_in2, inner2, t2_str_sizes,2);
		
		ArrayList<Tuple> list = fromSort_toArrayList(L1);
		ArrayList<Tuple> list2 = fromSort_toArrayList(L2);
		ArrayList<Tuple> list1_prime = fromSort_toArrayList(L1_prime);
		ArrayList<Tuple> list2_prime = fromSort_toArrayList(L2_prime);
	
//---------------------------------------------------------------------------
		
		// Permutation array Table 1_____________________________
		int N = list.size(); 
		int[] P = new int[N];


		int i = 0;
		for (Tuple t2 : list2) {
			int j = 0;
			for (Tuple t1: list) {
				if (compare(t1, t2)) {
					P[i] = j;
					break;
				}
				j++;
			}
			i++;
		}
		//_________________________________________________________
//-------------------------------------------------------------------------- 		
		
		// Permutation array Table 2_______________________________
		int M = list1_prime.size();
		int[] P_prime = new int[M];
		int[] B = new int[M]; // bit arrays (table T2)
		
		int k = 0;
		for (Tuple t2 : list2_prime) {
			int l = 0;
			for (Tuple t1 : list1_prime) {
				if (compare(t1, t2)) {
					P_prime[k] = l;
					break;
				}
				l++;
			}
			B[k] = 0;
			k++;
		}
		//___________________________________________________________
//--------------------------------------------------------------------------		
		
	// strict or equal inequality offset
		// method 1
		boolean[] eqOff_arr = new boolean[2];        
    
		for (int h = 0; h < 2; ++h) {
			// initialize eq off
			if (outFilter[h].op.attrOperator == AttrOperator.aopGE ||  
					outFilter[h].op.attrOperator == AttrOperator.aopLE) 
			{
				eqOff_arr[h] = true;
			} else {
				eqOff_arr[h] = false;
			}
		}

		int  eqOff = (!(eqOff_arr[0] && eqOff_arr[1]))?1:0;	

		// method 2
		int offset;
		if ((outFilter[0].op.toString() == "aopGE" || outFilter[0].op.toString() == "aopLE") && 
				(outFilter[1].op.toString() == "aopGE" || outFilter[1].op.toString() == "aopLE")) {
			offset = 0;
		}else {

			offset = 1;
		}
	
//--------------------------------------------------------------------------	
     // Offset array L1 - L1_prime__________________________________________________________
		int[] P1 = new int[N];  //Offsets arrays (L1 wrt L1_prime)
    
		int y = 0;
		for (Tuple t1 : list) {
			int x = 0;
			for (Tuple t1_prime: list1_prime) {
  			
				boolean var_allow = t1.getIntFld(outFilter[0].operand1.symbol.offset) ==
  								t1_prime.getIntFld(outFilter[0].operand2.symbol.offset);
  			
				if (var_allow) {
  				
					P1[y] = x;
					break;
				}
  					x++;
  				}
  				y++;
  			}
//_______________________________________________________________________________________

		
		
   	 // Offset array L2 - L2_prime____________________________________________________________
   	 	int[] P2 = new int[N];//Offsets arrays (L2 wrt L2_prime)
   	 
   	 	int a = 0;
   	 	for (Tuple t2 : list2) {
   	 		int b = 0;
   	 		for (Tuple t2_prime: list1_prime) {
  			
   	 		boolean var_allow = t2.getIntFld(outFilter[1].operand1.symbol.offset) ==
  								t2_prime.getIntFld(outFilter[1].operand2.symbol.offset);
  			
   	 		if (var_allow) {
  				
   	 			P2[a] = b;
   	 			break;
  			}
   	 		b++;
   	 	}
   	 		a++;
  	}	
   	 	//_____________________________________________________________________________________
   	

   

   	// IEJoin Algorithm ________________________________________________________
   	//Join Tuple and fill joinlist 
	int off_set2;
	int off_set1;
	
	for (int h = 0; h <list.size() ; h++) {
		off_set2 = P2[h]; // Offset2
		for (int n=0; n < Math.min(off_set2, list2.size()); n++) {
			
			B[P_prime[n]]=1; // Bit array filled
		}
		off_set1 = P1[P[h]]; // Offset 1
		
		for (int d = off_set1 + offset; d < list1_prime.size() ; d++) {
			// condition 
			
			if (B[d] == 1) { 
				Tuple tuple1 = new Tuple();
				tuple1 = null;
				Tuple tuple2 = new Tuple();
				tuple2 = null;
				tuple1 = list2.get(h);
				tuple2 = list2_prime.get(d);
				Projection.Join(tuple1, in, 
						tuple2, this.in2, 
						Jtuple, perm_mat, nOutFlds);
				Tuple jtuple = new Tuple(Jtuple);
				this.joinlist.add(jtuple);				
			}
			
		}
		
		
	}
	//_________________________________________________________________________
	
		
	}


	//--------------------------------------------------------------------------	USED METHODS
	// sort L2 and L2_prime
	public Sort sort_method_2(Sort L,AttrType in[], short in_len, Iterator iterator,short[] t_str_sizes, int i) throws SortException, IOException, IteratorBMException {
		
		// ascending
		int a = 0;
		if(i==1) {
			a = OutputFilter[1].operand1.symbol.offset;
		}
		else {
			a = OutputFilter[1].operand2.symbol.offset;
		}
		if (OutputFilter[1].op.attrOperator == AttrOperator.aopGT || OutputFilter[1].op.attrOperator == AttrOperator.aopGE) {

			TupleOrder descending = new TupleOrder(TupleOrder.Descending);
			L = new Sort (in, (short) in_len, t_str_sizes,
					(iterator.Iterator) iterator, a, descending, t_str_sizes[0], n_buf_pgs);
		} else if (OutputFilter[1].op.attrOperator == AttrOperator.aopLT || OutputFilter[1].op.attrOperator == AttrOperator.aopLE)
		{
			TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
			L = new Sort (in, (short) in_len, t_str_sizes,
					(iterator.Iterator) iterator, a, ascending, t_str_sizes[0], n_buf_pgs);
		}
		return L;
	}
	
	// sort L1 and L1_prime
	public Sort sort_method_1(Sort L,AttrType in[], short in_len, Iterator iterator,short[] t_str_sizes, int i) throws SortException, IOException, IteratorBMException {
		
		// ascending
		
		int a = 0;
		if(i==1) {
			a = OutputFilter[0].operand1.symbol.offset;
		}
		else {
			a = OutputFilter[0].operand2.symbol.offset;
		}
		if (OutputFilter[0].op.attrOperator == AttrOperator.aopGT || OutputFilter[0].op.attrOperator == AttrOperator.aopGE) {
		
			TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
			L = new Sort (in, (short) in_len, t_str_sizes,
					(iterator.Iterator) iterator, a, ascending, t_str_sizes[0], n_buf_pgs);
			
	
		// descending 
		} else if (OutputFilter[0].op.attrOperator == AttrOperator.aopLT ||OutputFilter[0].op.attrOperator == AttrOperator.aopLE)
		{
			
			TupleOrder descending = new TupleOrder(TupleOrder.Descending);
			L = new Sort (in, (short) in_len, t_str_sizes,
					(iterator.Iterator) iterator, a, descending, t_str_sizes[0], n_buf_pgs);
		}
		return L;
	}
	
	
	// From Sort to ArrayList 
	public ArrayList<Tuple> fromSort_toArrayList(Sort L) throws SortException, UnknowAttrType, LowMemException, JoinsException, IOException, Exception{ 
		
		ArrayList<Tuple> list = new ArrayList<Tuple>();
		Tuple L_tuple = null;
		while ((L_tuple = L.get_next()) != null)
		{	
			Tuple x = new Tuple(L_tuple); 
			list.add(x);
		}

		return list;
	}
	
	
	// another method to do eqOff but it seems that it doesn't work very well
	// eqOff value // like the other one used in IESelfJoinTwoPredicate
	public boolean tupleCond(Tuple t1, Tuple t2) throws UnknowAttrType, TupleUtilsException, IOException {
		 int result = 0;
         boolean result_bool = false;
         result = TupleUtils.CompareTupleWithTuple(new AttrType(AttrType.attrInteger),
	        		 									t2, OutputFilter[0].operand1.symbol.offset,
	        		 									t1, OutputFilter[0].operand1.symbol.offset
        );
         if(result ==0) {
        	 result_bool = false;
	     }{
	    	 result_bool = true;
	     }
	     
	     return result_bool;
	}
	
	public boolean compare(Tuple T1, Tuple T2) throws FieldNumberOutOfBoundException, IOException {
		
		if (
				T1.getIntFld(1) != T2.getIntFld(1) &&
				T1.getIntFld(2) != T2.getIntFld(2) &&
				T1.getIntFld(3) != T2.getIntFld(3) &&
				T1.getIntFld(4) != T2.getIntFld(4)) 
		{
			return false;
		}
		return true;
	}

	@Override
	public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
	InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
	LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {
		
		Tuple allowed = null;
		if(this.joinlist.size() > 0) {
			allowed = this.joinlist.get(0);
			this.joinlist.remove(0);
		}
		return allowed;
		
	} 


	@Override
	public void close() throws IOException, JoinsException, SortException, IndexException {
		if (!closeFlag) {
			//outer.close();
			//outer2.close();
			//inner.close();
			//inner2.close();
			closeFlag = true;
		}
	}


}
