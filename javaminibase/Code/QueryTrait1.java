package tests;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import bufmgr.PageNotReadException;
import global.AttrOperator;
import global.AttrType;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Tuple;
import index.IndexException;
import iterator.CondExpr;
import iterator.FileScan;
import iterator.FldSpec;
import iterator.IESelfJoinOnePred;
import iterator.IESelfJoinTwoPred;
import iterator.JoinSelf;
import iterator.JoinsException;
import iterator.LowMemException;
import iterator.NestedLoopException;
import iterator.NestedLoopsJoins;
import iterator.PredEvalException;
import iterator.RelSpec;
import iterator.SortException;
import iterator.TupleUtilsException;
import iterator.UnknowAttrType;
import iterator.UnknownKeyTypeException;

public class QueryTrait1 {
	
	private static final String FAIL = null;
	private String path;
	private String join;
	private String filename;
	private String query;
	private int count_rows;
	private long executiontime;
	
	
	public QueryTrait1(String query_path,String join,String query) {
		this.path = query_path;
		this.join = join;
		this.query= query;
	}

	
	public void execute_query() throws JoinsException, IndexException, InvalidTupleSizeException, InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {
		
		long startTime = System.nanoTime();
		QueryAnalyzer query_past = new QueryAnalyzer(path);
	    CondExpr[] outFilter = query_past.getOutFilter();
		   
	    Integer[] column_selected = query_past.getColumn_selected();

		    
	    String[] relations = query_past.getRelations();
		    
	    String innerRelation = new String(relations[0]);
	    String outerRelation = new String(relations[1]);

	    Tuple t = new Tuple();
	    t = null;
	    AttrType[] types_1 = {
	    		new AttrType(AttrType.attrInteger), 
	    		new AttrType(AttrType.attrInteger),
		    		new AttrType(AttrType.attrInteger), 
		    		new AttrType(AttrType.attrInteger)};

		    AttrType[] types_2 = {
		    		new AttrType(AttrType.attrInteger), 
		    		new AttrType(AttrType.attrInteger),
		    		new AttrType(AttrType.attrInteger), 
		    		new AttrType(AttrType.attrInteger)};

		    AttrType[] types_j = {
		    		new AttrType(AttrType.attrInteger), 
		    		new AttrType(AttrType.attrInteger)};

		    FldSpec[] proj = {
		    		new FldSpec(new RelSpec(RelSpec.innerRel), column_selected[1]), // R
		    		new FldSpec(new RelSpec(RelSpec.outer),column_selected[0]),}; // S

		    FldSpec[] projection = {
		        new FldSpec(new RelSpec(RelSpec.outer), 1),
		        new FldSpec(new RelSpec(RelSpec.outer), 2), 
		        new FldSpec(new RelSpec(RelSpec.outer), 3),
		        new FldSpec(new RelSpec(RelSpec.outer), 4),
		      };
		   
		    short []   sizes = new short[1];
			sizes[0] = (short) 0;
			
		    FileScan am = null;
		    FileScan am2 = null;
		    FileScan am_prime = null; 
		    FileScan am2_prime = null;
		 
		    try {
		        
		    am = new FileScan(outerRelation+".in", types_1, sizes, (short) 4, (short) 4, projection,null);
		    am2 = new FileScan(outerRelation+".in", types_1, sizes, (short) 4, (short) 4, projection,null);
		      
	        am_prime = new FileScan(innerRelation+".in", types_1, sizes, (short) 4, (short) 4, projection, null);
		    am2_prime = new FileScan(innerRelation+".in", types_1, sizes, (short) 4, (short) 4, projection, null);
		    
		    } catch (Exception e) {
		      System.err.println("" + e);
		    }
		NestedLoopsJoins nlj = null;
	    IESelfJoinOnePred iej_2a = null;

	  
	    if (join.equals("NLJ")) {

		
	        nlj = new NestedLoopsJoins(types_1, 4, null, types_2, 4, null, 10, am, innerRelation+".in", outFilter, null, proj, 2);
	    }
	    if (join.equals("IESelfJoinOnePred")) {

		
	    	iej_2a = new IESelfJoinOnePred(types_1, 4, sizes, 10, am, outFilter, null, proj, 2);
	    }

	    this.executiontime = (System.nanoTime() - startTime) ;
	    if (join.equals("NLJ")) {
	    	
	    	UsefulFunctions use = new UsefulFunctions();
			use.write_to_file(query+"_NLJ",nlj);
			this.count_rows = use.getNumRows();
	    }
	    if (join.equals("IESelfJoinOnePred")) {
	    	
	    	UsefulFunctions use = new UsefulFunctions();
			use.write_to_file(query+"_IESelfJoinOne",iej_2a);
			this.count_rows = use.getNumRows();
	    }

	}

	/** getters and setters 
	 * 
	 * @return
	 */
	public long gettime() {
		return executiontime;
	}

	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getJoin() {
		return join;
	}

	public void setJoin(String join) {
		this.join = join;
	}
	
	public int getRows() {
		return count_rows;
	}

}
