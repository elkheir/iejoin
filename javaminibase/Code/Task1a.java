package tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import bufmgr.PageNotReadException;
import global.AttrType;
import global.GlobalConst;
import global.RID;
import global.SystemDefs;
import heap.HFBufMgrException;
import heap.HFDiskMgrException;
import heap.Heapfile;
import heap.InvalidSlotNumberException;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Tuple;
import index.IndexException;
import iterator.CondExpr;
import iterator.FileScan;
import iterator.FldSpec;
import iterator.Iterator;
import iterator.JoinsException;
import iterator.LowMemException;
import iterator.NestedLoopsJoins;
import iterator.PredEvalException;
import iterator.RelSpec;
import iterator.SortException;
import iterator.TupleUtilsException;
import iterator.UnknowAttrType;
import iterator.UnknownKeyTypeException;


public class Task1a implements GlobalConst {
	
	private boolean OK = true;
	private boolean FAIL = false;
	
	
	public Task1a() throws InvalidSlotNumberException, InvalidTupleSizeException, HFDiskMgrException, HFBufMgrException, IOException {
		Query1a();
	}
	
	
	public void Query1a() throws IOException,
												  InvalidSlotNumberException, 
												  InvalidTupleSizeException, 
												  HFDiskMgrException, 
												  HFBufMgrException 
	{
		
		boolean status = OK;
		long startTime = System.currentTimeMillis();
		
		try {
			// read the query file so as to extract
			// relations names, projection and condition columns
			
			QueryAnalyzer query = new QueryAnalyzer("QueriesData"+"/"+"query_1a"+".txt");
			
			CondExpr [] outFilter  = new CondExpr[2];
			outFilter = query.getOutFilter();
			
			// types of the Inner relation attribute 
			AttrType SAttrTypes[] = {
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger)
			};

			
			short []   Ssizes = new short[1];
			Ssizes[0]=0;

			// types of the Outer relation attribute 
			AttrType [] RAttrTypes = {
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger),
					new AttrType(AttrType.attrInteger)
			};

			
			short  []  Rsizes = new short[1];
			Rsizes[0] = 0;

		
			// Select Projection out of S
			FldSpec [] Sprojection = {
					new FldSpec(new RelSpec(RelSpec.outer), 1),
					new FldSpec(new RelSpec(RelSpec.outer), 2),
					new FldSpec(new RelSpec(RelSpec.outer), 3),
					new FldSpec(new RelSpec(RelSpec.outer), 4),
			};

		
			
			FldSpec [] Projection = {
					new FldSpec(new RelSpec(RelSpec.outer), 
							query.getColumn_selected()[0]),
					new FldSpec(new RelSpec(RelSpec.innerRel),
							query.getColumn_selected()[1])
			};

			
			
			
			// create scan file 
			FileScan am = null;

			try {
				am = new FileScan(
						query.getRelations()[0] + ".in", 
						SAttrTypes, 
						Ssizes, 
						(short) 4, 
						(short) 4,
						Sprojection,
						null);
				
			}
			catch (Exception e) 
			{
				status = FAIL;
				System.err.println("" + e);
			}

			// Nested Loop Join
			NestedLoopsJoins nlj = null;
			try {
				nlj = new NestedLoopsJoins (SAttrTypes,
						4, 
						Ssizes,
						RAttrTypes, 
						4, 
						Rsizes,
						10,
						am,query.getRelations()[1] + ".in",
						outFilter,
						null, 
						Projection, 2);
			}
			
			catch (Exception e) {
				
				System.err.println ("*** Error preparing for nested_loop_join");
				System.err.println (""+e);
				e.printStackTrace();
				Runtime.getRuntime().exit(1);
			
			}

			UsefulFunctions use = new UsefulFunctions();
			use.write_to_file("query_1a",nlj);

			// Close NLJ 
			try {
				nlj.close();
			} catch (Exception e) {
				status = FAIL;
				e.printStackTrace();
			}

			if (status != OK) {
				//bail out
				Runtime.getRuntime().exit(1);
			}

		} catch (FileNotFoundException e) {
			status = FAIL;
			e.printStackTrace();
		}
		
		long elapsedTime = System.currentTimeMillis() - startTime;
		System.out.println("time in ms of Task 1a "+ elapsedTime);
	}
	
	public static void main(String argv[]) throws JoinsException, IndexException, InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException, LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception
	{
		
		System.out.println("QUERY 1a executed USING NLP using number of rows 1000");
		System.out.println("Tuples are in query_1a.txt file");
		JoinsDriver_ jjoin = new JoinsDriver_(1000);
		Task1a query1a_nlj = new Task1a();
		System.out.println("test passes succesfully");

	}
	
}