package iterator;
import iterator.*;
import java.util.Stack;

import java.io.IOException;

import bufmgr.PageNotReadException;
import global.AttrOperator;
import global.AttrType;
import global.RID;
import global.TupleOrder;
import heap.FieldNumberOutOfBoundException;
import heap.Heapfile;
import heap.InvalidTupleSizeException;
import heap.InvalidTypeException;
import heap.Scan;
import heap.Tuple;
import index.IndexException;
import java.util.ArrayList;
import java.util.ListIterator;

public class IESelfJoinTwoPred extends Iterator {

	private AttrType      _in1[];
	private   int        in1_len;
	private   Iterator  outer;
	private   Iterator  outer2;
	private   short t2_str_sizescopy[];
	private   CondExpr OutputFilter[];
	private   CondExpr RightFilter[];
	private   int        n_buf_pgs;        // # of buffer pages available.
	private   boolean        done,         // Is the join complete
	get_from_outer;                 		// if TRUE, a tuple is got from outer
	private   Tuple     outer_tuple;
	private   Tuple     Jtuple;           // Joined tuple  
	private   FldSpec   perm_mat[];
	private   int        nOutFlds;
	private   Heapfile  hf;

	
	private Sort L1;
	private Sort L2;
	
	private ArrayList<Tuple> joinlist;

	public IESelfJoinTwoPred( AttrType    in1[],    
			int     len_in1,           
			short   t1_str_sizes[],   
			int     amt_of_mem,        
			Iterator     am1,
			Iterator     am2,      
			CondExpr outFilter[],      
			CondExpr rightFilter[],    
			FldSpec   proj_list[],
			int        n_out_flds
			) throws SortException, LowMemException, JoinsException, Exception
	
	{

//---------------------------------------------------------------------------Inherited Steps
		_in1 = new AttrType[in1.length];
		System.arraycopy(in1,0,_in1,0,in1.length);
		in1_len = len_in1;

		outer = am1;
		outer2 = am2;
		Jtuple = new Tuple();
		OutputFilter = outFilter;
		RightFilter  = rightFilter;

		n_buf_pgs    = amt_of_mem; // num pages for heapfile
		done  = false;
		get_from_outer = true;

//---------------------------------------------------------------------------
		// Jtypes created join projection of result tuples as done before
		AttrType[] Jtypes = new AttrType[n_out_flds];

		short[] t_size;

		perm_mat = proj_list;
		nOutFlds = n_out_flds;
		try {
			t_size = TupleUtils.setup_op_tuple(Jtuple, Jtypes,
					in1, len_in1, in1, len_in1,
					t1_str_sizes, t1_str_sizes,
					proj_list, nOutFlds);
		}catch (TupleUtilsException e){
			throw new NestedLoopException(e, "TupleUtilsException is caught by IESelfJoins.java");
		}


//---------------------------------------------------------------------------
		// Sorting outer with sort and result in iterator	
		L1 = null;
		sort_method_L1(t1_str_sizes);
		L2 = null;
		sort_method_L2(t1_str_sizes);
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------From SORT TO ARRAYLIST
		Tuple tuple = new Tuple();
		tuple = null;
		ArrayList<Tuple> list = new ArrayList<Tuple>();
		while ((tuple = L1.get_next()) != null)
		{	
			Tuple x = new Tuple(tuple); 
			list.add(x);
		}
		// L2 to Array list "list"
		ArrayList<Tuple> list2 = new ArrayList<Tuple>();
		Tuple tuple1 = null;
		while ((tuple1  = L2.get_next()) != null)
		{	
			Tuple x = new Tuple(tuple1);
			list2.add(x);
		}
		
		/////// list2 done 
//---------------------------------------------------------------------------end OF CONVERSION
		
//---------------------------------------------------------------------------	
		// Creating Permutation array and Bit array
		// Permutation array
		int[] P = null;
		
		// Bit array  
		int[] B = null; 
		
		int N = 0;
		N = list.size();

		P = new int[N];
		B = new int[N];
		
//---------------------------------------------------------------------------INITIALISING VARIBLES 
		// Permutation array filled and 
		int i = 0;
		for (Tuple t2 : list2) {
			int j = 0;
			for (Tuple t1: list) {
				if (isnotEQUAL(t2, t1)) {
					P[i] = j;
					break;
				}
				j++;
			}
			i++;
		}
		
		//Bit array initialized with zeros
		for (int k =0; k < N;k++) {
			B[k] = 0;
		}	
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------Offset
		// strict or equal inequality offset
        // methdod_1
		
		boolean[] eqOff_arr = new boolean[2];

        for (int h = 0; h < 2; ++h) {
            // initialize eq off
            if (outFilter[h].op.attrOperator == AttrOperator.aopGE ||  
            		outFilter[h].op.attrOperator == AttrOperator.aopLE) 
            {
                eqOff_arr[h] = true;
            } else {
                eqOff_arr[h] = false;
            }
        }
        int  eqOff = (!(eqOff_arr[0] && eqOff_arr[1]))?1:0;
		//second method
		
		int offset;
		if (outFilter[0].op.toString() == "aopGE" || outFilter[0].op.toString() == "aopLE") {
			offset = 0;
		}else {
			offset = 1;
		} 
		
		
		Tuple t1 = new Tuple(); // tuple to join
		Tuple t2 = new Tuple(); // tuple to join
		
		int position_i; // tuple element of permutation list
		
		// another method to add offset
	
		
		joinlist = new ArrayList<Tuple>() ;
		
		// Join Tuples and fill the joinlist
		t1 = null;
		t2 = null;
		
		
//---------------------------------------------------------------------------RESEARCH REPORT ALGOTITHM		
		for (int k = 0; k < list.size() ; k++) {
			
			position_i = P[k] ;											// Bit array filled
			
			B[position_i] = 1 ;
			for (int j = position_i + offset; j < list.size() ; j++) {
	
				// condition to join tuples to tuple j with Bit array 
				if (B[j] == 1) { 
		
					t1 = list.get(j);
					t2 = list.get(position_i);
					
					Projection.Join(t1, _in1, 
									t2, _in1, 
									Jtuple, perm_mat, nOutFlds);
					
					Tuple jtuple = new Tuple(Jtuple);
					joinlist.add(jtuple);
				
				}
			}
		}

//--------------------------------------------------------------------------- END OF RESEARCH REPORT ALGOTITHM	
	}
	
	

	// Backup Function for join for debugging
	public void join(Tuple T1, Tuple T2, Tuple jtuple) throws FieldNumberOutOfBoundException, IOException {
		
		//>> Join Tuples
		int element1 = T1.getIntFld(perm_mat[0].offset);
		int element2 = T1.getIntFld(perm_mat[1].offset);
		jtuple.setIntFld(1, element1);
		jtuple.setIntFld(2, element2);
		
	}

	// Function get_next to get the next result tuple of the algorithm IESelfJoin
	public Tuple get_next() throws IOException, JoinsException, IndexException, InvalidTupleSizeException,
	InvalidTypeException, PageNotReadException, TupleUtilsException, PredEvalException, SortException,
	LowMemException, UnknowAttrType, UnknownKeyTypeException, Exception {
		

		Tuple allowed = null;
		if (joinlist.size() > 0) {
			allowed = joinlist.get(0);
			joinlist.remove(0);
		}
		return allowed;
		
	} 
	
	// SORT L2 >> OUTPUTFILTER 1
	public void sort_method_L2(short[] t1_str_sizes) throws SortException, IOException, IteratorBMException {
		
		// ascending
		if (OutputFilter[1].op.attrOperator == AttrOperator.aopGT || OutputFilter[1].op.attrOperator == AttrOperator.aopGE) {

			TupleOrder descending = new TupleOrder(TupleOrder.Descending);
			L2 = new Sort (_in1, (short) in1_len, t1_str_sizes,
					(iterator.Iterator) outer2, OutputFilter[1].operand1.symbol.offset, descending, t1_str_sizes[0], n_buf_pgs);
		} else if (OutputFilter[1].op.attrOperator == AttrOperator.aopLT || OutputFilter[1].op.attrOperator == AttrOperator.aopLE)
		{
			TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
			L2 = new Sort (_in1, (short) in1_len, t1_str_sizes,
					(iterator.Iterator) outer2, OutputFilter[1].operand1.symbol.offset, ascending, t1_str_sizes[0], n_buf_pgs);
		}
	}
	
	// SORT L1 >> OUTPUTFILTER 0
	public void sort_method_L1(short[] t1_str_sizes) throws SortException, IOException, IteratorBMException {
		
		// ascending
	
		if (OutputFilter[0].op.attrOperator == AttrOperator.aopGT || OutputFilter[0].op.attrOperator == AttrOperator.aopGE) {
			TupleOrder ascending = new TupleOrder(TupleOrder.Ascending);
			L1 = new Sort (_in1, (short) in1_len, t1_str_sizes,
					(iterator.Iterator) outer, OutputFilter[0].operand1.symbol.offset, ascending, t1_str_sizes[0], n_buf_pgs);
			
	
		
		// descending 
		}
		else if (OutputFilter[0].op.attrOperator == AttrOperator.aopLT ||OutputFilter[0].op.attrOperator == AttrOperator.aopLE)
		{ 
	
			TupleOrder descending = new TupleOrder(TupleOrder.Descending);
			L1 = new Sort (_in1, (short) in1_len, t1_str_sizes,
					(iterator.Iterator) outer, OutputFilter[0].operand1.symbol.offset, descending, t1_str_sizes[0], n_buf_pgs);
		}
	}
	

	 public boolean compare(Tuple t1, Tuple t2) throws UnknowAttrType, TupleUtilsException, IOException {
         int result = 0;
         boolean result_bool = false;
         result = TupleUtils.CompareTupleWithTuple(new AttrType(AttrType.attrInteger),
        		 									t2, OutputFilter[0].operand1.symbol.offset,
        		 									t1, OutputFilter[0].operand1.symbol.offset
            		 					);
         if(result ==0) {
           	 result_bool = true;
         }{
          	 result_bool = false;
         }

         return result_bool;
     }
	 
	public boolean isnotEQUAL(Tuple T1, Tuple T2) throws FieldNumberOutOfBoundException, IOException {
		if (
			T1.getIntFld(1) != T2.getIntFld(1) &&
			T1.getIntFld(2) != T2.getIntFld(2) &&
			T1.getIntFld(3) != T2.getIntFld(3) &&
			T1.getIntFld(4) != T2.getIntFld(4)) {
			
			return false;
		}
			return true;
		}
	 
	// close function to close scan of heapfile
	public void close() throws IOException, JoinsException, SortException, IndexException {
		if (!closeFlag) {

			try {
				outer.close();
				outer2.close();
			}catch (Exception e) {
				throw new JoinsException(e, "NestedLoopsJoin.java: error in closing iterator.");
			}
			closeFlag = true;
		}
	}
	
	
}
