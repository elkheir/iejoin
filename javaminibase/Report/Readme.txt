##
For this assignment, we created a new class called JoinDriver_ which contain an independent JoinsDriver, and main method to execute queries  
To test the different implementations, use ONLY eclipse and execute the main of the JoinDriver_. We could not use make files because of some directory conflicts for the path given in queries.
Be careful when running all the tests: a buffer manager error can be output soon. 

---------------------------------------------------------------------------------------------

QueryTrait1 : 
QueryTrait1.java is a class that takes in argument the path to the query and the Iterator in which we would like to execute our query. The method excute_query() calls the QueryAnalyzer so as to parse the query and then execute it using the given iterator.
This class takes only one predicate iterator :
             1.    Nested Loop Joins
            2.      IESelfJoinOnePred (explained after)

______________________________________________________________________________________________

QueryTrait2 : 
QueryTrait2.java is a class that takes in argument the path to the query and the Iterator in which we would like to execute our query, as well as the maximal number of rows.
The method execute_query() calls the QueryAnalyzer so as to parser the query and then execute it using the given iterator.
This class takes only two predicate iterator :
             1.    Nested Loop Joins
            2.      IESelfJoinTwoPred (explained after)
            3.    JoinSelf (explained after)

----------------------------------------------------------------------------------------------
See the Result of tuples after running class Task1a ( Task1b ) in output_query1a.txt ( output_query2a.txt ) files of Outputs Folder.

----------------------------------------------------------------------------------------------
 to execute a query with iterator of one predicate, you have to create an instance of QueryTrait1 with the right path to the query and a string of iterator name, here. After executing, the result will be saved in a file on Outputs folder as : outputs_”query to be executed”_”iterator chosen name”.txt
----------------------------------------------------------------------------------------------

 N.B : to execute a query iterator of two predicate, you have to create an instance of QueryTrait2 with the right path to the query and a string of iterator names, here. After executing, the result will be saved in a file on Outputs folder as : outputs_”query to be executed”_”iterator chosen name”.txt

For more details, see our gitlab repository: https://gitlab.eurecom.fr/elkheir/iejoin/-/blob/master/